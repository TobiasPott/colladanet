﻿using System;
using System.Xml.Serialization;

namespace ColladaNET.Elements
{

    [Serializable()]
    [XmlType(AnonymousType = true, Namespace = Collada.XMLNAMESPACE)]
    [XmlRoot(Namespace = Collada.XMLNAMESPACE, IsNullable = false)]
    public abstract partial class PrimitiveBase
    {
        [XmlElement("input")]
        public InputOffset[] input
        { get; set; }

        [XmlElement("extra")]
        public Extra[] extra
        { get; set; }

        [XmlAttribute(DataType = "NCName")]
        public string name
        { get; set; }

        [XmlAttribute()]
        public ulong count
        { get; set; }

        [XmlAttribute(DataType = "NCName")]
        public string material
        { get; set; }

    }

    [Serializable()]
    [XmlType(AnonymousType = true, Namespace = Collada.XMLNAMESPACE)]
    [XmlRoot(Namespace = Collada.XMLNAMESPACE, IsNullable = false)]
    public abstract partial class Primitive<T> : PrimitiveBase
    {
        [XmlElement("p")]
        public T p
        { get; set; }
    }

}