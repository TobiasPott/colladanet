﻿using System;
using System.Xml.Serialization;

namespace ColladaNET.Elements
{

    [Serializable()]
    [XmlType(AnonymousType = true, Namespace = Collada.XMLNAMESPACE)]
    [XmlRoot(Namespace = Collada.XMLNAMESPACE, IsNullable = false)]
    public partial class Polygons : PrimitiveBase
    {
        [XmlElement("p", typeof(string))]
        [XmlElement("ph", typeof(PH))]
        public object[] Items
        { get; set; }


        [Serializable()]
        [XmlType(AnonymousType = true, Namespace = Collada.XMLNAMESPACE)]
        public partial class PH
        {
            [XmlElement()]
            public string p
            { get; set; }

            [XmlElement("h")]
            public string[] h
            { get; set; }
        }
    }
    
}
