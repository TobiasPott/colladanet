﻿using ColladaNET.Elements;
using ColladaNET.Generation;
using System.Collections.Generic;
using UnityEngine;
using WInt.Structs;

namespace ColladaNET.Unity
{
    public static class TypeExtensions
    {
        public static Color ToColor(this Float4 input)
        { return new Color(input.X, input.Y, input.Z, input.W); }
        public static Vector4 ToVector4(this Float4 input)
        { return new Vector4(input.X, input.Y, input.Z, input.W); }
        public static Vector3 ToVector3(this Float3 input)
        { return new Vector3(input.X, input.Y, input.Z); }
        public static Vector2 ToVector2(this Float2 input)
        { return new Vector2(input.X, input.Y); }

        public static Float4 ToFloat4(this Color input)
        { return new Float4(input.r, input.g, input.b, input.a); }
        public static Float4 ToFloat4(this Vector4 input)
        { return new Float4(input.x, input.y, input.z, input.w); }
        public static Float3 ToFloat3(this Vector3 input)
        { return new Float3(input.x, input.y, input.z); }
        public static Float2 ToFloat2(this Vector2 input)
        { return new Float2(input.x, input.y); }
    }

    public class Suffix
    {
        public const string GeometryData = "-geo";
        public const string Scene = "-scene";
        public const string Material = "-mat";
        public const string Shader = "-fx";
        public const string Joints = "-Joints";
        public const string Weights = "-Weights";
        public const string Matrices = "-Matrices";
        public const string Controller = "Controller";
        public const string FileExtension = ".dae";
    }


    public class Dae : FormatBase
    {

        private VisualScene _scene = null;

        private COLLADAFactory _daeFactory;
        private List<DAEGeometryInputSemanticInfo> _semanticInfos = new List<DAEGeometryInputSemanticInfo>();


        public VisualScene Scene
        { get { return _scene; } }

        public override void Begin()
        {
            // create new factory for combined collada file
            _daeFactory = new COLLADAFactory();
            _daeFactory.Begin();
            // add the default visual scene to the combined factory
            _scene = _daeFactory.AddVisualScene(Suffix.Scene);
            // don't forget to convert this into a Preference!!
            // COLLADA.CompatibilityToBlender = true;

            // initial clear of arrays // might be moved to the finish to release memory 
            this.Clear();
        }

        private void Clear()
        {
            _positions.Clear();
            _normals.Clear();
            _texcoords.Clear();
            _colors.Clear();
            _vertices.Clear();
            _triangles.Clear();
            _objectMaterials.Clear();
            _objectTriangles.Clear();
            _materials.Clear();
            _semanticInfos.Clear();
        }

        #region Public Override - void - Object(string, params string[])
        /// <summary>
        /// defines a new object with the given name and stores the current starting indices
        /// </summary>
        /// <param name="objectName">name of the object</param>
        /// <param name="materials">optional list of material names associated with this object</param>
        public override void Object(string objectName, params MaterialData[] materials)
        {
            string[] matNames = new string[materials.Length];
            for (int m = 0; m < matNames.Length; m++)
                matNames[m] = materials[m].Name;

            _objectMaterials.Add(objectName, matNames);

            foreach (MaterialData mat in materials)
                if (!_materials.Contains(mat))
                    _materials.Add(mat);
            _objectTriangles.Add(objectName, new List<Triangle[]>());
        }
        #endregion

        public override void FinishObject()
        {
            this.BuildGeometries();
            this.BuildMaterials();
            this.Clear();
        }

        public override void GeometryData(IEnumerable<Vertex> vertices, IEnumerable<Float3> positions, IEnumerable<Float3> normals, IEnumerable<Float2> texcoords, IEnumerable<Float4> colors)
        {
            if (vertices != null)
            {
                _vertices.AddRange(vertices);
                _positions.AddRange(positions);
                _normals.AddRange(normals);
                _texcoords.AddRange(texcoords);
                _colors.AddRange(colors);
            }
        }

        public override void MeshData(string objectName, Triangle[][] triangles)
        {
            if (triangles != null)
            {
                foreach (Triangle[] subTriangles in triangles)
                {
                    if (subTriangles != null)
                        if (_objectTriangles.ContainsKey(objectName))
                            _objectTriangles[objectName].Add(subTriangles); // represents the list of triangles for each polylist/submesh of an object; each Triangle[] matches one submesh
                }
            }
        }


        public static int[] VerticesToIntArray(Triangle[] triangles, Vertex[] vertices)
        {
            List<int> primitives = new List<int>();
            foreach (Triangle tri in triangles)
            {
                vertices[tri.V1].AddIndicesToList(primitives);
                vertices[tri.V2].AddIndicesToList(primitives);
                vertices[tri.V3].AddIndicesToList(primitives);
            }
            return primitives.ToArray();
        }
        public static int[] VerticesToIntArray(IList<Triangle> triangles, IList<Vertex> vertices)
        {
            List<int> primitives = new List<int>();
            foreach (Triangle tri in triangles)
            {
                vertices[tri.V1].AddIndicesToList(primitives);
                vertices[tri.V2].AddIndicesToList(primitives);
                vertices[tri.V3].AddIndicesToList(primitives);
            }
            return primitives.ToArray();
        }

        // ! ! ! ! 
        // add a way to rebuild an objects hierarchy including the meshes
        //  ->  perhaps by rebuilding the complete hierarchy by default and allow a check for each Geometry object to find it's parent using a string path representation (e.g. XPath notation like)
        //  ->  

        protected override void BuildGeometries()
        {
            foreach (string nGeo in _objectMaterials.Keys)
            {
                this.BuildGeometry(nGeo, _daeFactory);
                // move to separate method to detach from geometry library building
                Float3 tls = Float3.Zero;
                Elements.Node node = _scene.Find(nGeo);
                if (node == null)
                {
                    node = new Elements.Node(nGeo, nGeo, nGeo);
                    _scene.AddNode(node);
                }

                // ! ! ! ! !
                // disabled instance geometry for debugging skinned meshes
                //  ->  need a way to determine what kind of instance should be added to the scene (perhaps do this inside the "BuildGeometry" method
                //  ->  or independent of the geometry building process itself

                // ! ! ! ! 
                // when exporting as geometry instance the x-axis is mirrored again
                //node.AddInstanceGeometry("#" + nGeo + Suffix.GeometryData, _objectMaterials[nGeo] == null ? new string[0] : _objectMaterials[nGeo]);
                // no x-axis mirroring occurs when exporting as instance controller
                node.AddInstanceController("#" + nGeo + Suffix.Controller, _objectMaterials[nGeo]);

                //_daeFactory.AddInstanceGeometry(_sceneIndex, node, "#" + nGeo + Suffix.GeometryData, _objectMaterials[nGeo] == null ? new string[0] : _objectMaterials[nGeo]);
            }

            // set a visual scene to be used as the instance in the collada.scene element
            string nScene = Suffix.Scene;
            _daeFactory.SetSceneInstanceVisualScene("", "", "#" + nScene);
        }
        protected void BuildGeometry(string objectName, COLLADAFactory factory)
        {
            List<DAEGeometryDataSourceInfo> dataSourceInfos = new List<DAEGeometryDataSourceInfo>();
            List<float[]> lData = new List<float[]>();

            dataSourceInfos.Add(new DAEGeometryDataSourceInfo(GeometryDataType.Position, 0));
            //lData.Add(Float3.ToFloatArray(_positions));
            lData.Add(Float3.ToFloatArray(_positions));

            if (_normals != null && _normals.Count > 0)
            {
                dataSourceInfos.Add(new DAEGeometryDataSourceInfo(GeometryDataType.Normal, 0));
                lData.Add(Float3.ToFloatArray(_normals));
            }
            if (_texcoords != null && _texcoords.Count > 0)
            {
                dataSourceInfos.Add(new DAEGeometryDataSourceInfo(GeometryDataType.TexCoord, 0));
                lData.Add(Float2.ToFloatArray(_texcoords));
            }
            if (_colors != null && _colors.Count > 0)
            {
                dataSourceInfos.Add(new DAEGeometryDataSourceInfo(GeometryDataType.VertexColor, 0));
                lData.Add(Float4.ToFloatArray(_colors));
            }

            Geometry geo = DAEGeometry.MeshDAE(objectName + Suffix.GeometryData, objectName, dataSourceInfos.ToArray(), lData.ToArray());
            for (int i = 0; i < this._objectTriangles[objectName].Count; i++)
            {
                Triangle[] tris = this._objectTriangles[objectName][i];
                int[] arrVCount = new int[tris.Length];
                for (int tInd = 0; tInd < tris.Length; tInd++)
                    arrVCount[tInd] = 3;

                DAEGeometry.AppendPolylist(objectName, _objectMaterials[objectName][i], _semanticInfos.ToArray(), arrVCount, Dae.VerticesToIntArray(tris, _vertices), geo.MeshDAE);
            }
            factory.AddGeometry(geo);

        }



        protected override void BuildMaterials()
        {
            for (int i = 0; i < _materials.Count; i++)
            {
                MaterialData mData = _materials[i];
                string nMat = mData.Name;
                string nFx = mData.Name + Suffix.Shader;

                _daeFactory.AddMaterial(nMat, nMat, "#" + nFx);

                EffectFx_COMMON fx = EffectFx_COMMON.GeneratePhong(new Type_ColorOrTexture("diffuse", mData.Diffuse),
                                                                    new Type_ColorOrTexture("emission", mData.Emission),
                                                                    new Type_ColorOrTexture("ambient", mData.Ambient),
                                                                    new Type_ColorOrTexture("specular", mData.Specular),
                                                                    new Type_FloatOrParam("shininess", mData.Shininess),
                                                                    new Type_ColorOrTexture("reflective", mData.Reflective),
                                                                    new Type_FloatOrParam("reflectivity", mData.Reflectivity),
                                                                    new Type_Transparent(fx_opaque_enum.RGB_ZERO, new Type_ColorOrTexture.Color("transparent", 1, 1, 1, 1)),
                                                                    new Type_FloatOrParam("transparency", 0),
                                                                    new Type_FloatOrParam("index_of_refraction", 0));
                _daeFactory.AddEffect(nFx, nMat, fx);
            }
        }

        public void AddSceneNode(ColladaNET.Elements.Node node)
        {
            _scene.AddNode(node);
        }
        public void AddController(ColladaNET.Elements.Controller ctrl)
        {
            _daeFactory.AddController(ctrl);
        }

        public void SetSemantics(Semantic[] semantics)
        {
            _semanticInfos.Clear();
            ulong texIndex = 0;
            ulong clrIndex = 0;
            for (int i = 0; i < semantics.Length; i++)
            {
                if (semantics[i] == Semantic.VERTEX)
                {
                    _semanticInfos.Add(new DAEGeometryInputSemanticInfo(Semantic.VERTEX, 0, 0));
                }
                else if (semantics[i] == Semantic.NORMAL)
                {
                    _semanticInfos.Add(new DAEGeometryInputSemanticInfo(Semantic.NORMAL, 0, 0));
                }
                else if (semantics[i] == Semantic.TEXCOORD)
                {
                    _semanticInfos.Add(new DAEGeometryInputSemanticInfo(Semantic.TEXCOORD, 0, texIndex));
                    texIndex++;
                }
                else if (semantics[i] == Semantic.COLOR)
                {
                    _semanticInfos.Add(new DAEGeometryInputSemanticInfo(Semantic.COLOR, 0, clrIndex));
                    clrIndex++;
                }
            }
        }

        public override void Finish(string outputPath = "")
        {

            COLLADA col = _daeFactory.End();
            if (!outputPath.EndsWith(Suffix.FileExtension))
                outputPath += Suffix.FileExtension;
            Debug.Log("Out to: " + outputPath);
            COLLADAIO.SaveToFile(col, outputPath);

        }

    }

}