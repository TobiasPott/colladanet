﻿using System;
using System.Collections.Generic;
//using UnityEngine;
using WInt.Structs;


namespace ColladaNET.Unity
{
    public abstract class FormatBase
    {

        protected List<Float3> _positions = new List<Float3>();
        protected List<Float3> _normals = new List<Float3>();
        protected List<Float2> _texcoords = new List<Float2>();
        protected List<Float4> _colors = new List<Float4>();
        protected List<Vertex> _vertices = new List<Vertex>();
        //protected List<Quad> _quads = new List<Quad>();
        protected List<Triangle> _triangles = new List<Triangle>();


        protected Dictionary<string, string[]> _objectMaterials = new Dictionary<string, string[]>();
        protected Dictionary<string, List<Triangle[]>> _objectTriangles = new Dictionary<string, List<Triangle[]>>();
        protected List<MaterialData> _materials = new List<MaterialData>();

        //public abstract void Start();
        public abstract void Begin();
        public abstract void Object(string objectName, params MaterialData[] materials);
        public abstract void FinishObject();

        #region Public - void - GeometryData(IEnumerable<Vertex>, IEnumerable<Float3>, IEnumerable<Float3>, IEnumerable<Float2>, IEnumerable<Float4>)
        /// <summary>
        /// appends vertices and geometry data (e.g. positions, normals, uvs and colors) to the factory
        /// </summary>
        /// <param name="vertices">vertices to add to the factory</param>
        /// <param name="positions">positions to add to the factory</param>
        /// <param name="normals">normals to add to the factory</param>
        /// <param name="texcoords">texcoords to add to the factory</param>
        /// <param name="colors">colors to add to the factory</param>
        public abstract void GeometryData(IEnumerable<Vertex> vertices, IEnumerable<Float3> positions, IEnumerable<Float3> normals, IEnumerable<Float2> texcoords, IEnumerable<Float4> colors);
        #endregion

        #region Public - void - MeshData(string, Triangle[][])
        /// <summary>
        /// appends mesh triangles to the factory
        /// </summary>
        /// <param name="objectName">object to associate the given triangles with</param>
        /// <param name="triangles">triangles to add to the factory</param>
        public abstract void MeshData(string objectName, Triangle[][] triangles);
        #endregion

        //protected abstract void Prepare();
        //protected abstract void BuildTexture();
        protected abstract void BuildGeometries();
        protected abstract void BuildMaterials();

        //public abstract void Build();
        public abstract void Finish(string outputPath = "");

    }

}