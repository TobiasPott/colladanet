﻿using ColladaNET.Generation;
using ColladaNET.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using WInt.Structs;

namespace ColladaNET.UnityEditor
{

    public class ColladaExport
    {
        private const bool __LOG = true;

        // matrix to transform mesh vertices and normals (actually no idea why this is required inside Unity)
        public static readonly Matrix4x4 ScaleNegativeX = Matrix4x4.Scale(new Vector3(-1, 1, 1));
        public static readonly Matrix4x4 ScaleNegativeY = Matrix4x4.Scale(new Vector3(1, -1, 1));
        public static readonly Matrix4x4 ScaleNegativeZ = Matrix4x4.Scale(new Vector3(1, 1, -1));
        public static readonly Matrix4x4 RotateY180 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 180, 0), Vector3.one);
        public static readonly Matrix4x4 RotateX180 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(180, 0, 0), Vector3.one);
        public static readonly Matrix4x4 RotateZ180 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, 180), Vector3.one);

        public static void GetFromSelection(List<Mesh> meshes, List<Material[]> materials, List<SkinnedMeshRenderer> skinnedMeshes)
        {
            if (meshes == null && materials == null)
                return;

            foreach (GameObject go in Selection.gameObjects)
            {
                MeshFilter mf = go.GetComponent<MeshFilter>();
                if (mf != null)
                {
                    meshes.Add(mf.sharedMesh);
                    Renderer rend = go.GetComponent<Renderer>();
                    if (rend != null)
                        materials.Add(rend.sharedMaterials);
                    skinnedMeshes.Add(null);
                }
                else
                {
                    SkinnedMeshRenderer smr = go.GetComponent<SkinnedMeshRenderer>();
                    if (smr != null)
                    {
                        // use baked mesh to export animated mesh in it's current state
                        //if (Application.isPlaying)
                        //{
                        //    Mesh m = new Mesh();
                        //    m.name = smr.sharedMesh.name;
                        //    smr.BakeMesh(m);
                        //    meshes.Add(m);
                        //}
                        // else

                        // default way to export skinned mesh to preserve skinning and original pose
                        {
                            meshes.Add(smr.sharedMesh);
                            materials.Add(smr.sharedMaterials);
                            skinnedMeshes.Add(smr);
                        }
                    }
                }

            }

        }
        public static void GetFromSelectionHierarchy(GameObject[] gameObjects, List<Mesh> meshes, List<Material[]> materials, List<SkinnedMeshRenderer> skinnedMeshes)
        {

            if (gameObjects == null || meshes == null || materials == null)
                return;

            for (int i = 0; i < gameObjects.Length; i++)
            {
                for (int j = 0; j < gameObjects.Length; j++)
                {
                    if (gameObjects[j] == null || i == j)
                        continue;
                    else if (gameObjects[i].transform.IsChildOf(gameObjects[j].transform))
                        gameObjects[i] = null;
                }
            }

            for (int i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i] == null)
                    continue;

                MeshFilter[] mfs = gameObjects[i].GetComponentsInChildren<MeshFilter>();
                foreach (MeshFilter mf in mfs)
                {
                    meshes.Add(mf.sharedMesh);
                    Renderer rend = mf.GetComponent<Renderer>();
                    if (rend != null)
                        materials.Add(rend.sharedMaterials);
                    skinnedMeshes.Add(null);
                }

                SkinnedMeshRenderer[] smrs = gameObjects[i].GetComponentsInChildren<SkinnedMeshRenderer>();
                foreach (SkinnedMeshRenderer smr in smrs)
                {
                    // use baked mesh to export animated mesh in it's current state
                    //if (Application.isPlaying)
                    //{
                    //    Mesh m = new Mesh();
                    //    m.name = smr.sharedMesh.name;
                    //    smr.BakeMesh(m);
                    //    meshes.Add(m);
                    //}
                    // else

                    // default way to export skinned mesh to preserve skinning and original pose
                    {
                        meshes.Add(smr.sharedMesh);
                        materials.Add(smr.sharedMaterials);
                        skinnedMeshes.Add(smr);
                    }
                }
            }

        }

        [Obsolete("GetMeshFromSelection is obsolete and replaced by GetFromSelection method which covers receiving of all required object types.")]
        public static Mesh GetMeshFromSelection()
        {
            if (Selection.activeGameObject != null)
            {
                GameObject selGo = Selection.activeGameObject;
                MeshFilter mf = selGo.GetComponent<MeshFilter>();
                if (mf != null)
                    return mf.sharedMesh;

                SkinnedMeshRenderer smr = selGo.GetComponent<SkinnedMeshRenderer>();
                if (smr != null)
                {
                    Mesh m = new Mesh();
                    m.name = selGo.name;
                    smr.BakeMesh(m);
                    return m;
                }

            }

            return null;
        }
        [Obsolete("GetMaterialsFromSelection is obsolete and replaced by GetFromSelection method which covers receiving of all required object types.")]
        public static Material[] GetMaterialsFromSelection()
        {
            if (Selection.activeGameObject != null)
            {
                GameObject selGo = Selection.activeGameObject;
                Renderer rend = selGo.GetComponent<Renderer>();
                if (rend != null)
                    return rend.sharedMaterials;
            }

            return null;
        }
        [Obsolete("GetSkinnedMeshRendererFromSelection is obsolete and replaced by GetFromSelection method which covers receiving of all required object types.")]
        public static SkinnedMeshRenderer GetSkinnedMeshRendererFromSelection()
        {
            if (Selection.activeGameObject != null)
            {
                GameObject selGo = Selection.activeGameObject;
                SkinnedMeshRenderer smr = selGo.GetComponent<SkinnedMeshRenderer>();

                return smr;
            }
            return null;
        }


        [MenuItem("Tools/Read Mesh from Dae &#R")]
        public static void ReadColladaFile()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            Debug.Log(path);
            COLLADA colladaFile = null;
            Exception ex = null;
            COLLADAIO.LoadFromFile(path, out colladaFile, out ex);
            Debug.Log(colladaFile + ": " + ex);
        }


        private static System.Diagnostics.Stopwatch _sw = null;

        [MenuItem("Tools/Export Mesh as Dae &#E")]
        public static void ExportAsDae()
        {
            if (_sw == null)
                _sw = new System.Diagnostics.Stopwatch();
            _sw.Reset();
            _sw.Start();

            EditorUtility.ClearProgressBar();

            Dae dae = new Dae();
            dae.Begin();

            List<Mesh> meshes = new List<Mesh>();
            List<Material[]> materials = new List<Material[]>();
            List<SkinnedMeshRenderer> skinnedMeshes = new List<SkinnedMeshRenderer>();
            // ColladaExport.GetFromSelection(meshes, materials, skinnedMeshes);
            ColladaExport.GetFromSelectionHierarchy(Selection.gameObjects, meshes, materials, skinnedMeshes);

            GameObject[] gos = Selection.gameObjects.Distinct().ToArray();
            // if only one top-level object is selected work on its children
            if (gos.Length == 1)
            {
                GameObject go = gos[0];
                gos = new GameObject[go.transform.childCount];
                for (int i = 0; i < go.transform.childCount; i++)
                    gos[i] = go.transform.GetChild(i).gameObject;
            }

            //for (int i = 0; i < gos.Length; i++)
            //    if (gos[i] != null)
            //        dae.AddSceneNode(BuildNodesFromHierarchy(gos[i].transform, true));

            ColladaExport.LogTime("Hierarchy BLD");

            for (int i = 0; i < meshes.Count; i++)
            {
                ColladaExport.AddToDae(dae, meshes[i], materials[i], skinnedMeshes[i]);

                ColladaExport.LogTime("AddToDae");
                dae.FinishObject();
                ColladaExport.LogTime("FinishObject");
            }

            //Elements.Node n = dae.Scene.Find("EthanHips/EthanSpine/EthanRightUpLeg/EthanRightLeg");
            //if (n != null)
            //    Debug.Log("Found node: " + n.name);

            string dirPath = Application.dataPath; // Path.GetDirectoryName(path);
            ColladaExport.LogTime("File BLD");
            string path = Path.Combine(dirPath, "Export");
            dae.Finish(path);
            ColladaExport.LogTime("File IO");

            AssetDatabase.Refresh(ImportAssetOptions.Default);
            _sw.Stop();
        }


        private static List<Float3> _cachePos = new List<Float3>();
        private static List<Float3> _cacheNml = new List<Float3>();
        private static List<Float2> _cacheTex = new List<Float2>();
        private static List<Float4> _cacheClr = new List<Float4>();

        // ! ! ! !
        // requires a Dae object which had "Begin" called before "AddToDae" is called
        //  ->  add the XPath-like hierarchy path of the mesh to the parameters and append it to the respective nodes
        public static void AddToDae(Dae dae, Mesh m, Material[] materials, SkinnedMeshRenderer smr)
        {
            if (m != null)
            {
                //Mesh m = (Mesh)Selection.activeObject;
                Vector3[] mPos = m.vertices;
                Vector3[] mNml = m.normals;
                Vector2[] mTex = m.uv;
                Vector2[] mTex2 = m.uv2;
                Vector2[] mTex3 = m.uv3;
                Vector2[] mTex4 = m.uv4;
                Color[] mClr = m.colors;
                int[] mTris = m.triangles;

                Float3[] oPos = new Float3[mPos.Length];
                Float3[] oNml = new Float3[mNml.Length];
                Float2[] oTex = new Float2[mTex.Length];
                Float2[] oTex2 = new Float2[mTex2.Length];
                Float2[] oTex3 = new Float2[mTex3.Length];
                Float2[] oTex4 = new Float2[mTex4.Length];
                Float4[] oClr = new Float4[mClr.Length];

                for (int i = 0; i < mPos.Length; i++)
                    oPos[i] = (mPos[i]).ToFloat3();
                for (int i = 0; i < mNml.Length; i++)
                    oNml[i] = (mNml[i]).ToFloat3().Normalized;
                for (int i = 0; i < mTex.Length; i++)
                    oTex[i] = mTex[i].ToFloat2();
                for (int i = 0; i < mTex2.Length; i++)
                    oTex2[i] = mTex2[i].ToFloat2();
                for (int i = 0; i < mTex3.Length; i++)
                    oTex3[i] = mTex3[i].ToFloat2();
                for (int i = 0; i < mTex4.Length; i++)
                    oTex4[i] = mTex4[i].ToFloat2();
                for (int i = 0; i < mClr.Length; i++)
                    oClr[i] = mClr[i].ToFloat4();

                List<Float3> lPos = new List<Float3>(oPos);
                List<Float3> lNml = new List<Float3>(oNml);
                List<Float2> lTex = new List<Float2>(oTex);
                List<Float4> lClr = new List<Float4>(oClr);

                lTex.AddRange(oTex2);
                lTex.AddRange(oTex3);
                lTex.AddRange(oTex4);

                ColladaExport.LogTime("Data COL");

                lPos = lPos.Distinct().ToList();
                lPos.Sort(Float3Comparer.Instance);
                lNml = lNml.Distinct().ToList();
                lNml.Sort(Float3Comparer.Instance);
                lTex = lTex.Distinct().ToList();
                lTex.Sort(Float2Comparer.Instance);
                lClr = lClr.Distinct().ToList();
                lClr.Sort(Float4Comparer.Instance);

                ColladaExport.LogTime("Data OPT");

                int chCntNormals = m.normals != null ? 1 : 0;
                int chCntUVs = m.uv != null ? 1 : 0;
                if (m.uv2 != null && m.uv2.Length > 0) chCntUVs++;
                if (m.uv3 != null && m.uv3.Length > 0) chCntUVs++;
                if (m.uv4 != null && m.uv4.Length > 0) chCntUVs++;
                int chCntColors = (m.colors != null && m.colors.Length > 0) ? 1 : 0;

                Semantic[] semantics = new Semantic[1 + chCntNormals + chCntUVs + chCntColors];
                int ind = 0;
                semantics[0] = Semantic.VERTEX;
                ind++;
                for (int i = 0; i < chCntNormals; i++)
                    semantics[i + ind] = Semantic.NORMAL;
                ind += chCntNormals;
                for (int i = 0; i < chCntUVs; i++)
                    semantics[i + ind] = Semantic.TEXCOORD;
                ind += chCntUVs;
                for (int i = 0; i < chCntColors; i++)
                    semantics[i + ind] = Semantic.COLOR;
                ind += chCntColors;

                // ! ! ! ! !
                // think about making this multi-threaded (and several other parts to speed up the process)
                // not the slowest part (as File IO just breaks the deal when writing ASCII data)
                //  -> possibly in steps of 3 (each task does one triangle)

                Vertex[] oVerts = new Vertex[mTris.Length];
                Triangle[] oTris = new Triangle[mTris.Length / 3];
                Triangle[][] oTrisSub = new Triangle[m.subMeshCount][];

                int vIndex = 0;
                int vIndexOffset = 0;
                for (int s = 0; s < m.subMeshCount; s++)
                {
                    int[] sTris = m.GetIndices(s);
                    oTrisSub[s] = new Triangle[sTris.Length / 3];

                    for (int i = 0; i < sTris.Length; i++)
                    {
                        int mIndVtx = sTris[i];
                        Vertex v = Vertex.Create(chCntNormals, chCntUVs, chCntColors);
                        v.Pos = lPos.BinarySearch(oPos[mIndVtx], Float3Comparer.Instance);

                        if (chCntNormals != 0)
                            v.Nml[0] = lNml.BinarySearch(oNml[mIndVtx], Float3Comparer.Instance);
                        if (chCntUVs > 0)
                            v.Tex[0] = lTex.BinarySearch(oTex[mIndVtx], Float2Comparer.Instance);
                        if (chCntUVs > 1)
                            v.Tex[1] = lTex.BinarySearch(oTex2[mIndVtx], Float2Comparer.Instance);
                        if (chCntUVs > 2)
                            v.Tex[2] = lTex.BinarySearch(oTex3[mIndVtx], Float2Comparer.Instance);
                        if (chCntUVs > 3)
                            v.Tex[3] = lTex.BinarySearch(oTex4[mIndVtx], Float2Comparer.Instance);
                        if (chCntColors != 0)
                            v.Clr[0] = lClr.BinarySearch(oClr[mIndVtx], Float4Comparer.Instance);

                        sTris[i] = vIndexOffset + i;
                        oVerts[vIndex] = v;
                        vIndex++;

                        // check if it is the last of the tree triangle indices
                        //if (i % 3 == 2)
                        //    oTrisSub[s][i / 3] = new Triangle(sTris[i], sTris[i - 1], sTris[i - 2]);
                        if (i % 3 == 2)
                            oTrisSub[s][i / 3] = new Triangle(sTris[i - 2], sTris[i - 1], sTris[i]);
                    }
                    vIndexOffset = vIndex;
                }

                string path = Application.dataPath;
                string dirPath = Application.dataPath; // Path.GetDirectoryName(path);

                // build materials
                MaterialData[] meshMats = BuildMaterialData(materials);
                ColladaExport.LogTime("Mesh BLD");

                ColladaNET.Elements.Node skeletonRoot = null;
                ColladaNET.Elements.Controller ctrl = null;
                if (BuildSkinController(smr, m, lPos, oPos, out ctrl, out skeletonRoot))
                {
                    dae.AddSceneNode(skeletonRoot);
                    dae.AddController(ctrl);
                }
                ColladaExport.LogTime("Skin BLD");

                dae.Object(m.name, meshMats);
                ColladaExport.LogTime("Object CRT");
                dae.GeometryData(oVerts, lPos, lNml, lTex, lClr);
                ColladaExport.LogTime("GeometryData CRT");
                dae.MeshData(m.name, oTrisSub);
                ColladaExport.LogTime("Mesh CRT");

                dae.SetSemantics(semantics);

            }
        }

        public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
        {
            // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
            Quaternion q = new Quaternion();
            q.w = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] + m[1, 1] + m[2, 2])) / 2;
            q.x = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] - m[1, 1] - m[2, 2])) / 2;
            q.y = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] + m[1, 1] - m[2, 2])) / 2;
            q.z = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] - m[1, 1] + m[2, 2])) / 2;
            q.x *= Mathf.Sign(q.x * (m[2, 1] - m[1, 2]));
            q.y *= Mathf.Sign(q.y * (m[0, 2] - m[2, 0]));
            q.z *= Mathf.Sign(q.z * (m[1, 0] - m[0, 1]));
            return q;
        }
        private static ColladaNET.Elements.Node FillNodeHierarchy(Transform meshParent, Transform rootBone, ColladaNET.Elements.Node childNode = null)
        {
            Transform smrParent = meshParent;
            Transform skelParent = rootBone;
            ColladaNET.Elements.Node skelNode = childNode;
            do
            {
                if (skelParent == smrParent)
                    break;
                skelParent = skelParent.parent;
                if (skelParent == null || skelParent == smrParent)
                    break;

                ColladaNET.Elements.Node newSkelNode = new Elements.Node(skelParent.name, skelParent.name, skelParent.name);
                newSkelNode.type = Elements.NodeType.NODE;

                // ! ! ! !
                // negative scale on X maybe only applied to the topmost node (as it is passed down to every other node
                Vector3 localPos = ScaleNegativeX * skelParent.localPosition;
                Vector3 localEulers = skelParent.localEulerAngles;
                Vector3 localScale = skelParent.localScale;


                Quaternion localRot = QuaternionFromMatrix(Matrix4x4.TRS(Vector3.zero, Quaternion.Inverse(skelParent.localRotation), Vector3.one));
                localEulers = localRot.eulerAngles;

                //Quaternion localRotRoot = QuaternionFromMatrix(Matrix4x4.TRS(Vector3.zero, smr.bones[i].localRotation, Vector3.one) * ScaleNegativeY);
                //localEulers = localRotRoot.eulerAngles;


                newSkelNode.AddTranslate(localPos.x, localPos.y, localPos.z);
                newSkelNode.AddRotate(localEulers.x, localEulers.y, localEulers.z);
                newSkelNode.AddScale(localScale.x, localScale.y, localScale.z);
                newSkelNode.AddNode(skelNode);

                skelNode = newSkelNode;
            }
            while (smrParent != skelParent);

            return skelNode;
        }

        private static Elements.Node UpdateBoneNodes(Transform rootBone, Transform[] bones)
        {
            int boneIndex = Array.IndexOf<Transform>(bones, rootBone);
            if (boneIndex != -1)
            {
                Transform bone = bones[boneIndex];
                string boneName = bone.name;

                ColladaNET.Elements.Node node = new ColladaNET.Elements.Node(boneName, boneName, boneName, ColladaNET.Elements.NodeType.JOINT);
                node.AddTranslate("translate", bone.localPosition.x, bone.localPosition.y, bone.localPosition.z);
                if (!Mathf.Approximately(bone.localEulerAngles.x, 0.0f))
                    node.AddRotate("rotateX", 1.0f, 0.0f, 0.0f, bone.localEulerAngles.x); // negate the angle (actually I dunno why)
                if (!Mathf.Approximately(bone.localEulerAngles.y, 0.0f))
                    node.AddRotate("rotateY", 0.0f, 1.0f, 0.0f, bone.localEulerAngles.y); // negate the angle (actually I dunno why)
                if (!Mathf.Approximately(bone.localEulerAngles.z, 0.0f))
                    node.AddRotate("rotateZ", 0.0f, 0.0f, 1.0f, bone.localEulerAngles.z); // negate the angle (actually I dunno why)
                                                                                          //node.AddScale("scale", bone.localScale.x, bone.localScale.y, bone.localScale.z);

                // bones[boneIndex] = null;
                List<ColladaNET.Elements.Node> childNodes = new List<ColladaNET.Elements.Node>();
                for (int i = 0; i < bones.Length; i++)
                {
                    if (bone.Equals(bones[i]))
                        continue;
                    bone = bones[i];
                    boneName = bone.name;

                    //Vector3 relPos = rootBone.TransformPoint(bone.position);
                    Vector3 relPos = rootBone.position - bone.position;
                    Vector3 relRotation = (Quaternion.Inverse(rootBone.rotation) * bone.rotation).eulerAngles;

                    ColladaNET.Elements.Node childNode = new ColladaNET.Elements.Node(boneName, boneName, boneName, ColladaNET.Elements.NodeType.JOINT);
                    childNode.AddTranslate("translate", relPos.x, relPos.y, relPos.z);
                    //if (!Mathf.Approximately(relRotation.x, 0.0f))
                    //    childNode.AddRotate("rotateX", 1.0f, 0.0f, 0.0f, relRotation.x); // negate the angle (actually I dunno why)
                    //if (!Mathf.Approximately(relRotation.y, 0.0f))
                    //    childNode.AddRotate("rotateY", 0.0f, 1.0f, 0.0f, relRotation.y); // negate the angle (actually I dunno why)
                    //if (!Mathf.Approximately(relRotation.z, 0.0f))
                    //    childNode.AddRotate("rotateZ", 0.0f, 0.0f, 1.0f, relRotation.z); // negate the angle (actually I dunno why)
                    //childNode.AddScale("scale", bone.localScale.x, bone.localScale.y, bone.localScale.z);
                    //ColladaNET.Elements.Node childNode = ColladaExport.BuildNodesFromBones(rootBone.GetChild(i), bones);
                    if (childNode != null)
                        childNodes.Add(childNode);
                }
                node.Nodes = childNodes.ToArray();

                return node;
            }
            return null;
        }

        private static void LogTime(string tag)
        {
            if (__LOG)
                Debug.Log("Time [" + tag + "]: " + (_sw.ElapsedMilliseconds / 1000.0f));
        }

        public struct BoneInfluence : IComparable<BoneInfluence>
        {
            public static readonly BoneInfluence Empty = new BoneInfluence(-1, 0.0f);


            private int _bone;
            private float _weight;

            public int Bone
            { get { return _bone; } }

            public float Weight
            { get { return _weight; } }


            public BoneInfluence(int bone, float weight)
            {
                _bone = bone;
                _weight = weight;
            }

            public int CompareTo(BoneInfluence other)
            {
                int cmpBone = this._bone.CompareTo(other._bone);
                if (cmpBone == 0)
                    return this._weight.CompareTo(other._weight);
                return cmpBone;
            }

        }
        public struct VertexWeight : IComparable<VertexWeight>
        {
            private int _vertex;
            private List<BoneInfluence> _influences;


            public int Vertex
            {
                get { return _vertex; }
                set { _vertex = value; }
            }
            public BoneInfluence this[int index]
            {
                get { return _influences[index]; }
                set { _influences[index] = value; }
            }
            public int Count
            { get { return (_influences != null) ? _influences.Count : 0; } }

            public VertexWeight(int vertex)
            {
                _vertex = vertex;
                _influences = new List<BoneInfluence>();
            }

            public void Add(BoneInfluence influence)
            {
                if (!_influences.Contains(influence))
                    _influences.Add(influence);
            }

            public int CompareTo(VertexWeight other)
            {
                int cmpVetex = this._vertex.CompareTo(other._vertex);
                return cmpVetex;
            }
        }


        private static bool BuildSkinController(SkinnedMeshRenderer smr, Mesh m, List<Float3> lPos, Float3[] oPos,
                                                out ColladaNET.Elements.Controller controller,
                                                out ColladaNET.Elements.Node skeletonRoot)
        {
            controller = null;
            skeletonRoot = null;
            if (smr != null)
            {
                string ctrlName = m.name + Suffix.Controller;

                Elements.Skin skin = new Elements.Skin("#" + m.name + Suffix.GeometryData, null, null);
                controller = new Elements.Controller(ctrlName, string.Empty, skin);

                skin.bind_shape_matrix = DAEConverter.FromArray<Matrix4x4>(Matrix4x4.identity);

                Dictionary<string, Elements.Node> dictBones = new Dictionary<string, Elements.Node>();
                string[] jointNames = new string[smr.bones.Length];
                Matrix4x4[] jointMatrices = new Matrix4x4[smr.bones.Length];

                // ! ! ! ! !
                //  ->  check if a bone with the given path already exists in the hierarchy 
                //      ->  thus should require to determine the upmost shared parent (below the selection) to work

                for (int i = 0; i < smr.bones.Length; i++)
                {
                    Vector3 localPos = smr.bones[i].localPosition;
                    Vector3 localEulers = smr.bones[i].localEulerAngles;
                    Vector3 localScale = smr.bones[i].localScale;

                    jointNames[i] = smr.bones[i].name;
                    jointMatrices[i] = smr.sharedMesh.bindposes[i].transpose;

                    Elements.Node bNode = new Elements.Node(jointNames[i], jointNames[i], jointNames[i]);
                    bNode.type = Elements.NodeType.JOINT;
                    dictBones.Add(smr.bones[i].GetPath().Replace(smr.rootBone.GetPath(), smr.rootBone.name), bNode);

                    bNode.AddTranslate(localPos.x, localPos.y, localPos.z);
                    bNode.AddRotate("rotateX", 1.0f, 0.0f, 0.0f, localEulers.x);
                    bNode.AddRotate("rotateY", 0.0f, 1.0f, 0.0f, localEulers.y);
                    bNode.AddRotate("rotateZ", 0.0f, 0.0f, 1.0f, localEulers.z);
                    //bNode.AddRotate(localEulers.x, localEulers.y, localEulers.z);
                    //bNode.AddScale(localScale.x, localScale.y, localScale.z);
                }


                List<string> keys = dictBones.Keys.ToList();
                keys.Sort();
                for (int i = 0; i < keys.Count; i++)
                {
                    string iKey = keys[i];
                    for (int j = 1; j < keys.Count; j++)
                    {
                        string jKey = keys[j];
                        if (jKey.Substring(0, jKey.LastIndexOf('/')).Equals(iKey))
                            dictBones[iKey].AddNode(dictBones[jKey]);
                    }
                }

                skeletonRoot = dictBones[keys[0]];
                skeletonRoot = ColladaExport.FillNodeHierarchy(smr.transform.parent, smr.rootBone, skeletonRoot);

                BoneWeight[] bWeights = smr.sharedMesh.boneWeights;
                List<int> jointWeightCount = new List<int>();
                List<float> jointWeights = new List<float>();

                Dictionary<int, VertexWeight> dictVertexWeights = new Dictionary<int, VertexWeight>();

                Vector3[] vertices = smr.sharedMesh.vertices;
                for (int i = 0; i < bWeights.Length; i++)
                {
                    // missed the matrix multiplication of the source vertices
                    Float3 vertex = oPos[i];
                    int vertexIndex = lPos.BinarySearch(vertex, Float3Comparer.Instance);

                    if (!dictVertexWeights.ContainsKey(vertexIndex))
                        dictVertexWeights.Add(vertexIndex, new VertexWeight(vertexIndex));

                    VertexWeight vw = dictVertexWeights[vertexIndex];
                    BoneWeight bw = bWeights[i];

                    if (bw.weight0 > 0.0f)
                    {
                        jointWeights.Add(bw.weight0);
                        vw.Add(new BoneInfluence(bw.boneIndex0, bw.weight0));
                    }
                    if (bw.weight1 > 0.0f)
                    {
                        jointWeights.Add(bw.weight1);
                        vw.Add(new BoneInfluence(bw.boneIndex1, bw.weight1));
                    }
                    if (bw.weight2 > 0.0f)
                    {
                        jointWeights.Add(bw.weight2);
                        vw.Add(new BoneInfluence(bw.boneIndex2, bw.weight2));
                    }
                    if (bw.weight3 > 0.0f)
                    {
                        jointWeights.Add(bw.weight3);
                        vw.Add(new BoneInfluence(bw.boneIndex3, bw.weight3));
                    }


                }
                // remove duplicate weights from the list
                jointWeights = jointWeights.Distinct().ToList();
                jointWeights.Sort();

                List<int> jointVertexWeight = new List<int>();

                // ! ! ! ! !
                // the weight indices aren't looked up correctly!!
                //  ->  the matrices for the bones are fucked up (copying from 3ds max exported makes it look correctly (excluding the base rotation of the object)

                for (int i = 0; i < lPos.Count; i++)
                {
                    if (dictVertexWeights.ContainsKey(i))
                    {
                        VertexWeight infl = dictVertexWeights[i];
                        for (int bI = 0; bI < infl.Count; bI++)
                        {
                            BoneInfluence bInfl = infl[bI];
                            jointVertexWeight.Add(bInfl.Bone);
                            jointVertexWeight.Add(jointWeights.BinarySearch(bInfl.Weight));
                        }
                        jointWeightCount.Add(infl.Count);
                    }
                }


                string vCount = DAEConverter.FromList<int>(jointWeightCount);
                string vertexWeights = DAEConverter.FromList<int>(jointVertexWeight);

                Elements.Source sJoints = DAESkin.GenerateSkinSourceJoints(ctrlName, jointNames);
                Elements.Source sMatrices = DAESkin.GenerateSkinSourceMatrices(ctrlName, UnityHelper.ToFloatArray(jointMatrices));
                Elements.Source sWeights = DAESkin.GenerateSkinSourceWeights(ctrlName, jointWeights.ToArray());

                skin.sources = new Elements.Source[] { sJoints, sMatrices, sWeights };
                Elements.InputLocal il0 = DAEHelper.GenerateSemantic(ctrlName, Semantic.JOINT);
                Elements.InputLocal il1 = DAEHelper.GenerateSemantic(ctrlName, Semantic.INV_BIND_MATRIX);
                Elements.SkinJoints skinJoints = new Elements.SkinJoints(new Elements.InputLocal[] { il0, il1 });
                skin.joints = skinJoints;

                Elements.InputLocalOffset ilo0 = DAEHelper.GenerateSemanticOffset(ctrlName, Semantic.JOINT, 0, 0, 0);
                Elements.InputLocalOffset ilo1 = DAEHelper.GenerateSemanticOffset(ctrlName, Semantic.WEIGHT, 0, 0, 1);
                Elements.SkinVertexWeights skinVertexWeights = new Elements.SkinVertexWeights(new Elements.InputLocalOffset[] { ilo0, ilo1 });
                skinVertexWeights.count = (ulong)dictVertexWeights.Keys.Count;
                skinVertexWeights.v = vertexWeights;
                skinVertexWeights.vcount = vCount;
                skin.vertex_weights = skinVertexWeights;
                return true;
            }
            return false;
        }

        // ! ! ! ! 
        // only works from EditorCode as Texture paths can only be queried with AssetDatabase
        // -> to make it work during runtime, textures would be required to be saved from objects (which then might be limited due to non-read-access)
        private static MaterialData[] BuildMaterialData(Material[] materials)
        {
            MaterialData[] results = null;
            //dae.NewObject(m.name);
            if (materials != null)
            {
                results = new MaterialData[materials.Length];
                //foreach (Material mat in mats)
                for (int i = 0; i < materials.Length; i++)
                {
                    results[i] = new MaterialData(materials[i].name);
                    FillMaterialData(results[i], materials[i]);
                }
            }
            return results;
        }
        private static void FillMaterialData(MaterialData mData, Material mat)
        {
            // material Data extraction based on the Unity5 standard & standard specular materials
            //mData.Name = mat.name;
            mData.Ambient = new float[] { 0, 0, 0, 1 };
            mData.Reflective = new float[] { 0, 0, 0, 1 };

            if (mat.HasProperty("_Color"))
            {
                Color cDiffuse = mat.GetColor("_Color");
                mData.Diffuse = new float[] { cDiffuse.r, cDiffuse.g, cDiffuse.b, cDiffuse.a };
            }
            else
                mData.Diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };

            /*
            if (mat.HasProperty("_MainTex"))
            {
                // map extraction is has delayed supported atm, as it requires reading of the Texture Asset
                Texture mDiffuse = mat.GetTexture("_MainTex");
                string path = AssetDatabase.GetAssetPath(mDiffuse);
                Debug.Log("DiffuseMap: " + path);
                mData.DiffuseMap = path;
            }
            */

            if (mat.HasProperty("_EmissionColor"))
            {
                Color cEmission = mat.GetColor("_EmissionColor");
                mData.Emission = new float[] { cEmission.r, cEmission.g, cEmission.b, cEmission.a };
            }
            else
                mData.Emission = new float[] { 0, 0, 0, 0 };
            /*
            if (mat.HasProperty("_EmissionMap"))
            {
                Texture mEmission = mat.GetTexture("_EmissionMap");
                mData.EmissionMap = string.Empty;
            }
            */
            if (mat.HasProperty("_SpecColor"))
            {
                Color cSpecular = mat.GetColor("_SpecColor");
                mData.Specular = new float[] { cSpecular.r, cSpecular.g, cSpecular.b, cSpecular.a };
            }
            else
                mData.Specular = new float[] { 0, 0, 0, 1 };

            /*
            if (mat.HasProperty("_SpecGlossMap"))
            {
                Texture mSpecGloss = mat.GetTexture("_SpecGlossMap");
                mData.SpecularMap = string.Empty;
            }
            */
            // Normal mapping not supported directly by the Collada format
            //float fNormal = mats[i].GetFloat("_BumpScale");
            //Texture mapNormal = mats[i].GetTexture("_BumpMap");

            if (mat.HasProperty("_Glossiness"))
            {
                float fShininess = mat.GetFloat("_Glossiness");
                mData.Shininess = fShininess;
            }
            else
                mData.Reflectivity = 0;

        }


        private static Elements.Node BuildNodesFromHierarchy(Transform root, params Transform[] excluded)
        {

            string name = root.name;
            Elements.Node node = new Elements.Node(name, name, name);
            node.AddTranslate("translate", -root.localPosition.x, root.localPosition.y, root.localPosition.z);

            if (!Mathf.Approximately(root.localEulerAngles.x, 0.0f))
                node.AddRotate("rotateX", 1.0f, 0.0f, 0.0f, -root.localEulerAngles.x); // negate the angle (actually I dunno why)
            if (!Mathf.Approximately(root.localEulerAngles.y, 0.0f))
                node.AddRotate("rotateY", 0.0f, 1.0f, 0.0f, -root.localEulerAngles.y); // negate the angle (actually I dunno why)
            if (!Mathf.Approximately(root.localEulerAngles.z, 0.0f))
                node.AddRotate("rotateZ", 0.0f, 0.0f, 1.0f, -root.localEulerAngles.z); // negate the angle (actually I dunno why)
            node.AddScale("scale", root.localScale.x, root.localScale.y, root.localScale.z);

            List<ColladaNET.Elements.Node> childNodes = new List<ColladaNET.Elements.Node>();
            for (int i = 0; i < root.childCount; i++)
            {
                Elements.Node childNode = BuildNodesFromHierarchy(root.GetChild(i), excluded);
                if (childNode != null)
                    childNodes.Add(childNode);
            }
            node.Nodes = childNodes.ToArray();

            return node;

        }
        private static Elements.Node BuildNodesFromHierarchy(Transform root, bool asJoint)
        {
            string name = root.name;
            Elements.Node node = new Elements.Node(name, name, name, asJoint ? Elements.NodeType.JOINT : Elements.NodeType.NODE);
            node.AddTranslate("translate", -root.localPosition.x, root.localPosition.y, root.localPosition.z);
            if (!Mathf.Approximately(root.localEulerAngles.x, 0.0f))
                node.AddRotate("rotateX", 1.0f, 0.0f, 0.0f, root.localEulerAngles.x); // negate the angle (actually I dunno why)
            if (!Mathf.Approximately(root.localEulerAngles.y, 0.0f))
                node.AddRotate("rotateY", 0.0f, 1.0f, 0.0f, root.localEulerAngles.y); // negate the angle (actually I dunno why)
            if (!Mathf.Approximately(root.localEulerAngles.z, 0.0f))
                node.AddRotate("rotateZ", 0.0f, 0.0f, 1.0f, root.localEulerAngles.z); // negate the angle (actually I dunno why)
            node.AddScale("scale", root.localScale.x, root.localScale.y, root.localScale.z);

            List<ColladaNET.Elements.Node> childNodes = new List<ColladaNET.Elements.Node>();
            for (int i = 0; i < root.childCount; i++)
            {
                Elements.Node childNode = BuildNodesFromHierarchy(root.GetChild(i), asJoint);
                if (childNode != null)
                    childNodes.Add(childNode);
            }
            node.Nodes = childNodes.ToArray();

            return node;
        }

    }

}