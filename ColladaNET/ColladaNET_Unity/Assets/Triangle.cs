﻿
namespace ColladaNET.Unity
{
    /// <summary>
    /// Class to store a triangular face's data (indices, color, texture)
    /// </summary>
    public class Triangle
    {

        #region Fields
        // private 
        /// <summary>
        /// private field storing the indices of the triangles vertices
        /// </summary>
        private int[] _vertices = new int[] { -1, -1, -1 };

        #endregion

        #region Properties
        // public
        #region Public Property - int - V1
        /// <summary>
        /// gets or sets the index of the third vertex
        /// </summary>
        public int V1
        {
            get { return _vertices[0]; }
            set { _vertices[0] = value; }
        }
        #endregion

        #region Public Property - int - V2
        /// <summary>
        /// gets or sets the index of the third vertex
        /// </summary>
        public int V2
        {
            get { return _vertices[1]; }
            set { _vertices[1] = value; }
        }
        #endregion

        #region Public Property - int - V3
        /// <summary>
        /// gets or sets the index of the third vertex
        /// </summary>
        public int V3
        {
            get { return _vertices[2]; }
            set { _vertices[2] = value; }
        }
        #endregion

        #region Public Property - int[] - Vertices
        /// <summary>
        /// gets the indices of the all vertices (first, second, third)
        /// </summary>
        public int[] Vertices
        { get { return _vertices; } }
        #endregion

        #endregion


        #region Ctors

        #region Ctor()
        /// <summary>
        /// creates a new instance
        /// </summary>
        public Triangle()
        { }
        #endregion

        #region Ctor(int, int, int)
        /// <summary>
        /// creates a new instance
        /// </summary>
        /// <param name="v1">index of the triangle's first vertex</param>
        /// <param name="v2">index of the triangle's second vertex</param>
        /// <param name="v3">index of the triangle's third vertex</param>
        public Triangle(int v1, int v2, int v3)
        {
            _vertices[0] = v1;
            _vertices[1] = v2;
            _vertices[2] = v3;
        }
        #endregion

        #region Ctor(Triangle)
        /// <summary>
        /// creates a new instance
        /// </summary>
        /// <param name="v1">index of the triangle's first vertex</param>
        public Triangle(Triangle source)
        {
            _vertices[0] = source._vertices[0];
            _vertices[1] = source._vertices[1];
            _vertices[2] = source._vertices[2];
        }
        #endregion

        #endregion


        #region Indexers

        #region Indexer - int - [int]
        /// <summary>
        /// gets or sets the [n]-th vertex' index value (vertex count is zero based)
        /// </summary>
        /// <param name="index">triangle relative index of the vertex</param>
        /// <returns>vertex' index value</returns>
        public int this[int index]
        {
            get
            {
                index = index % 3;
                return _vertices[index];
            }
            set
            {
                index = index % 3;
                _vertices[index] = value;
            }
        }
        #endregion

        #endregion

    }

}