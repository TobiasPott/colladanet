﻿using ColladaNET.Elements;
using System;


namespace ColladaNET.Unity
{
    // might be moved to the ColladaNET assembly
    public class MaterialData
    {
        public static readonly Type MaterialDataType = typeof(MaterialData);

        protected string name;

        protected float[] _diffuse; // add a map field
        protected string _diffuseMap;

        protected float[] _emission; // add a map field
        protected string _emissionMap;

        protected float[] _ambient; // add a map field

        protected float[] _reflective; // add a map field
        protected string _reflectiveMap;
        protected float _reflectivity;

        protected float[] _specular; // add a map field
        protected string _specularMap;
        protected float _shininess;

        protected fx_opaque_enum _opaqueness;
        protected float[] _transparent; // add a map field
        protected string _transparentMap;
        protected float _transparency;
        protected float _indexOfRefraction;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public float[] Diffuse
        {
            get { return _diffuse; }
            set { _diffuse = value; }
        }
        public string DiffuseMap
        {
            get { return _diffuseMap; }
            set { _diffuseMap = value; }
        }
        public float[] Emission
        {
            get { return _emission; }
            set { _emission = value; }
        }
        public string EmissionMap
        {
            get { return _emissionMap; }
            set { _emissionMap = value; }
        }
        public float[] Ambient
        {
            get { return _ambient; }
            set { _ambient = value; }
        }
        public float[] Reflective
        {
            get { return _reflective; }
            set { _reflective = value; }
        }
        public string ReflectiveMap
        {
            get { return _reflectiveMap; }
            set { _reflectiveMap = value; }
        }
        public float Reflectivity
        {
            get { return _reflectivity; }
            set { _reflectivity = value; }
        }
        public fx_opaque_enum Opaqueness
        {
            get { return _opaqueness; }
            set { _opaqueness = value; }
        }
        public float[] Transparent
        {
            get { return _transparent; }
            set { _transparent = value; }
        }
        public string TransparentMap
        {
            get { return _transparentMap; }
            set { _transparentMap = value; }
        }
        public float Transparency
        {
            get { return _transparency; }
            set { _transparency = value; }
        }
        public float IndexOfRefraction
        {
            get { return _indexOfRefraction; }
            set { _indexOfRefraction = value; }
        }
        public float[] Specular
        {
            get { return _specular; }
            set { _specular = value; }
        }
        public string SpecularMap
        {
            get { return _specularMap; }
            set { _specularMap = value; }
        }
        public float Shininess
        {
            get { return _shininess; }
            set { _shininess = value; }
        }


        public MaterialData(string name)
        {
            Name = name;
        }

    }
}