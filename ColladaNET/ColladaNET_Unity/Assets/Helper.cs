﻿using System.Collections.Generic;
using UnityEngine;
using WInt.Structs;

namespace ColladaNET
{

    public static class UnityHelper
    {

        public static Elements.Node Find(this Elements.VisualScene scene, string nodePath)
        {
            nodePath = nodePath.Trim('/');
            int sepIndex = nodePath.IndexOf('/');
            string curLevel = nodePath;

            if (sepIndex != -1)
            {
                curLevel = nodePath.Substring(0, sepIndex);
                nodePath = nodePath.Substring(sepIndex);
            }
            else
                nodePath = nodePath.Substring(curLevel.Length);

            // check if the path exceeds the current node level
            foreach (Elements.Node child in scene.Nodes)
            {
                if (child.name.Equals(curLevel))
                {
                    if (nodePath.Length > 1)
                    {
                        Elements.Node result = child.Find(nodePath);
                        if (result != null)
                            return result;
                    }
                    return child;
                }
            }
            return null;
        }
        public static Elements.Node Find(this Elements.Node node, string nodePath)
        {
            nodePath = nodePath.Trim('/');
            int sepIndex = nodePath.IndexOf('/');
            string curLevel = nodePath;

            if (sepIndex != -1)
            {
                curLevel = nodePath.Substring(0, sepIndex);
                nodePath = nodePath.Substring(sepIndex);
            }
            else
                nodePath = nodePath.Substring(curLevel.Length);

            // check if the path exceeds the current node level
            foreach (Elements.Node child in node.Nodes)
            {
                if (child.name.Equals(curLevel))
                {
                    if (nodePath.Length > 1)
                    {
                        Elements.Node result = child.Find(nodePath);
                        if (result != null)
                            return result;
                    }
                    return child;
                }
            }
            return null;
        }

        public static string GetPath(this Transform current)
        {
            if (current.parent == null)
                return "/" + current.name;
            return current.parent.GetPath() + "/" + current.name;
        }

        /// <summary>
        /// converts an array of Float2 to a sequential double array containing all position data (x0, y0, x1, y1, x2, y2, etc.)
        /// </summary>
        /// <param name="source">source array containing Vector2 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(Vector2[] source)
        {
            double[] result = new double[source.Length * 2];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 2;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float2 to a sequential double array containing all position data (x0, y0, x1, y1, x2, y2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Vector2 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(IList<Vector2> source)
        {
            double[] result = new double[source.Count * 2];

            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 2;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
            }

            return result;
        }

        /// <summary>
        /// converts an array of Float3 to a sequential double array containing all position data (x0, y0, z0, x1, y1, z1, x2, y2, etc.)
        /// </summary>
        /// <param name="source">source array containing Vector3 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(Vector3[] source)
        {
            double[] result = new double[source.Length * 3];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 3;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
                result[resI + 2] = source[i].z;
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float3 to a sequential double array containing all position data (x0, y0, z0, x1, y1, z1, x2, y2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Vector3 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(IList<Vector3> source)
        {
            double[] result = new double[source.Count * 3];

            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 3;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
                result[resI + 2] = source[i].z;
            }

            return result;
        }

        /// <summary>
        /// converts an array of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source array containing Vector4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(Vector4[] source)
        {
            double[] result = new double[source.Length * 4];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 4;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
                result[resI + 2] = source[i].z;
                result[resI + 3] = source[i].w;
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Vector4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(IList<Vector4> source)
        {
            double[] result = new double[source.Count * 4];

            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 4;
                result[resI] = source[i].x;
                result[resI + 1] = source[i].y;
                result[resI + 2] = source[i].z;
                result[resI + 3] = source[i].w;
            }

            return result;
        }

        /// <summary>
        /// converts an array of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source array containing Color elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(Color[] source)
        {
            double[] result = new double[source.Length * 4];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 4;
                result[resI] = source[i].r;
                result[resI + 1] = source[i].g;
                result[resI + 2] = source[i].b;
                result[resI + 3] = source[i].a;
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Color elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(IList<Color> source)
        {
            double[] result = new double[source.Count * 4];

            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 4;
                result[resI] = source[i].r;
                result[resI + 1] = source[i].g;
                result[resI + 2] = source[i].b;
                result[resI + 3] = source[i].a;
            }

            return result;
        }

        /// <summary>
        /// converts an array of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source array containing Matrix4x4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(Matrix4x4[] source)
        {
            double[] result = new double[source.Length * 16];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 16;
                for (int m = 0; m < 16; m++)
                    result[resI + m] = source[i][m];
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float4 to a sequential double array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Matrix4x4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static double[] ToDoubleArray(IList<Matrix4x4> source)
        {
            double[] result = new double[source.Count * 16];
            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 16;
                for (int m = 0; m < 16; m++)
                    result[resI + m] = source[i][m];
            }

            return result;
        }

        /// <summary>
        /// converts an array of Float4 to a sequential float array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source array containing Matrix4x4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static float[] ToFloatArray(Matrix4x4[] source)
        {
            float[] result = new float[source.Length * 16];

            for (int i = 0; i < source.Length; i++)
            {
                int resI = i * 16;
                for (int m = 0; m < 16; m++)
                    result[resI + m] = source[i][m];
            }

            return result;
        }
        /// <summary>
        /// converts a IList of Float4 to a sequential float array containing all position data (r0, g0, b0, a0, r1, g1, b1, a1, r2, g2, b2, etc.)
        /// </summary>
        /// <param name="source">source IList containing Matrix4x4 elements</param>
        /// <returns>array containing a sequential list of the source's data</returns>
        public static float[] ToFloatArray(IList<Matrix4x4> source)
        {
            float[] result = new float[source.Count * 16];
            for (int i = 0; i < source.Count; i++)
            {
                int resI = i * 16;
                for (int m = 0; m < 16; m++)
                    result[resI + m] = source[i][m];
            }

            return result;
        }

    }

    public class Float2Comparer : IComparer<Float2>
    {

        private static Float2Comparer _instance = new Float2Comparer();
        public static Float2Comparer Instance
        { get { return _instance; } }


        public int Compare(Float2 lh, Float2 rh)
        { return CompareFloat2(lh, rh); }

        private Float2Comparer()
        { }

        public static int CompareFloat2(Float2 lh, Float2 rh)
        {
            // return lh.X.CompareTo(rh.X) + lh.Y.CompareTo(rh.Y);
            int xCmp = lh.X.CompareTo(rh.X);
            if (xCmp == 0)
                return lh.Y.CompareTo(rh.Y);
            return xCmp;
        }

    }
    public class Float3Comparer : IComparer<Float3>
    {

        private static Float3Comparer _instance = new Float3Comparer();
        public static Float3Comparer Instance
        { get { return _instance; } }


        public int Compare(Float3 lh, Float3 rh)
        { return CompareFloat3(lh, rh); }

        private Float3Comparer()
        { }

        public static int CompareFloat3(Float3 lh, Float3 rh)
        {
            //return lh.X.CompareTo(rh.X) + lh.Y.CompareTo(rh.Y) + lh.Z.CompareTo(rh.Z);
            int xCmp = lh.X.CompareTo(rh.X);
            if (xCmp == 0)
            {
                int yCmp = lh.Y.CompareTo(rh.Y);
                if (yCmp == 0)
                    return lh.Z.CompareTo(rh.Z);
                return yCmp;
            }
            return xCmp;
        }
        //public static int CompareFloat3(Float3 lh, Float3 rh)
        //{
        //    //return lh.X.CompareTo(rh.X) + lh.Y.CompareTo(rh.Y) + lh.Z.CompareTo(rh.Z);
        //    int xCmp = lh.Z.CompareTo(rh.Z);
        //    if (xCmp == 0)
        //    {
        //        int yCmp = lh.Y.CompareTo(rh.Y);
        //        if (yCmp == 0)
        //            return lh.X.CompareTo(rh.X);
        //        return yCmp;
        //    }
        //    return xCmp;
        //}

    }
    public class Float4Comparer : IComparer<Float4>
    {

        private static Float4Comparer _instance = new Float4Comparer();
        public static Float4Comparer Instance
        { get { return _instance; } }


        public int Compare(Float4 lh, Float4 rh)
        { return CompareFloat4(lh, rh); }

        private Float4Comparer()
        { }

        public static int CompareFloat4(Float4 lh, Float4 rh)
        {
            //return lh.X.CompareTo(rh.X) + lh.Y.CompareTo(rh.Y) + lh.Z.CompareTo(rh.Z) + lh.W.CompareTo(rh.W);
            int xCmp = lh.X.CompareTo(rh.X);
            if (xCmp == 0)
            {
                int yCmp = lh.Y.CompareTo(rh.Y);
                if (yCmp == 0)
                {
                    int zCmp = lh.Z.CompareTo(rh.Z);
                    if (zCmp == 0)
                        return lh.W.CompareTo(rh.W);
                    return zCmp;
                }
                return yCmp;
            }
            return xCmp;
        }

    }

    public static class FloatExt
    {
        public static void Append(this List<float> dest, List<Float2> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                dest.Add(source[i].X);
                dest.Add(source[i].Y);
            }
        }
        public static void Append(this List<float> dest, List<Float3> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                dest.Add(source[i].X);
                dest.Add(source[i].Y);
                dest.Add(source[i].Z);
            }
        }
        public static void Append(this List<float> dest, List<Float4> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                dest.Add(source[i].X);
                dest.Add(source[i].Y);
                dest.Add(source[i].Z);
                dest.Add(source[i].W);
            }
        }
    }

}