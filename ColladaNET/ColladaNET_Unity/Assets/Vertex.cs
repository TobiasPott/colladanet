﻿using System;
using System.Collections.Generic;

namespace ColladaNET.Unity
{

    /// <summary>
    /// Structure for a 4 component index vertex (position, normal, texcoord, color)
    /// </summary>
    public struct Vertex : IEquatable<Vertex>
    {

        #region Fields
        // public
        /// <summary>
        /// public field storing the vertex' position index 
        /// </summary>
        public int Pos; // = -1;
                        /// <summary>
                        /// public field storing the vertex' normal index 
                        /// </summary>
        public int[] Nml; // = -1;
                          /// <summary>
                          /// public field storing the vertex' texcoord index 
                          /// </summary>
        public int[] Tex; // = -1;
                          /// <summary>
                          /// public field storing the vertex' color index 
                          /// </summary>
        public int[] Clr; // = -1;

        #endregion


        #region Properties
        public int Size
        { get { return Nml.Length + Tex.Length + Clr.Length + 1; } }

        #endregion


        #region Ctors

        #region Ctor(int, int, int, int)
        /// <summary>
        /// creates a new instance
        /// </summary>
        /// <param name="position">position index of the vertex</param>
        /// <param name="normal">normal index of the vertex</param>
        /// <param name="uv1">texcoord index of the vertex</param>
        /// <param name="color">color index of the vertex</param>
        public Vertex(int position, int normal, int uv1, int color)
        {
            this.Pos = position;
            this.Nml = new int[] { normal };
            this.Tex = new int[] { uv1 };
            this.Clr = new int[] { color };
        }
        public Vertex(int position, int normal, int uv1, int uv2, int color)
        {
            this.Pos = position;
            this.Nml = new int[] { normal };
            this.Tex = new int[] { uv1, uv2 };
            this.Clr = new int[] { color };
        }
        public Vertex(int position, int normal, int uv1, int uv2, int uv3, int color)
        {
            this.Pos = position;
            this.Nml = new int[] { normal };
            this.Tex = new int[] { uv1, uv2, uv3 };
            this.Clr = new int[] { color };
        }
        public Vertex(int position, int normal, int uv1, int uv2, int uv3, int uv4, int color)
        {
            this.Pos = position;
            this.Nml = new int[] { normal };
            this.Tex = new int[] { uv1, uv2, uv3, uv4 };
            this.Clr = new int[] { color };
        }

        public static Vertex Create(int chNormals, int chUVs, int chColors)
        {
            Vertex v = new Vertex();
            v.Pos = -1;
            v.Nml = new int[chNormals];
            v.Tex = new int[chUVs];
            v.Clr = new int[chColors];
            return v;
        }
        #endregion

        #region Ctor(Vertex)
        /// <summary>
        /// creates a new instance
        /// </summary>
        /// <param name="src">source vertex</param>
        public Vertex(Vertex src)
        {
            this.Pos = src.Pos;
            this.Nml = src.Nml;
            this.Tex = src.Tex;
            this.Clr = src.Clr;
        }
        #endregion
        #endregion

        public void AddIndicesToList(List<int> list)
        {
            list.Add(this.Pos);
            list.AddRange(this.Nml);
            list.AddRange(this.Tex);
            list.AddRange(this.Clr);
        }


        #region Override Methods
        // public
        #region Public Override - string - ToString()
        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object</returns>
        public override string ToString()
        {
            return string.Format("{0} [{1}] [{2}] [{3}]", Pos, Nml.Length, Tex.Length, Clr.Length);
        }
        #endregion

        #endregion
        public bool Equals(Vertex other)
        {
            return this == other;
        }
        public static bool operator ==(Vertex v1, Vertex v2)
        {
            if (v1.Pos == v2.Pos)
                if (ArrayEquals<int>(v1.Nml, v2.Nml))
                    if (ArrayEquals<int>(v1.Tex, v2.Tex))
                        if (ArrayEquals<int>(v1.Clr, v2.Clr))
                            return true;
            return false;
        }
        public static bool operator !=(Vertex v1, Vertex v2)
        {
            return !(v1 == v2);
        }
        public static bool ArrayEquals<T>(T[] a1, T[] a2)
        {
            if (a1 != null && a2 != null)
            {
                if (a1.Length == a2.Length)
                {
                    for (int i = 0; i < a1.Length; i++)
                        if (!a1[i].Equals(a2[i]))
                            return false;
                    return true;
                }
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType().Equals(typeof(Vertex)))
                return ((Vertex)obj).Equals(this);
            return false;
        }
        public override int GetHashCode()
        {
            int hashCode = Pos.GetHashCode();
            if (Nml != null)
                for (int i = 0; i < Nml.Length; i++)
                    hashCode += Nml[i].GetHashCode();
            if (Tex != null)
                for (int i = 0; i < Tex.Length; i++)
                    hashCode += Tex[i].GetHashCode();
            if (Clr != null)
                for (int i = 0; i < Clr.Length; i++)
                    hashCode += Clr[i].GetHashCode();
            //return Pos.GetHashCode() + Nml.GetHashCode() + Tex.GetHashCode() + Clr.GetHashCode();
            return hashCode;
        }
    }

}