﻿using System;
using WInt.Structs;

namespace ColladaNET.Unity
{
    public struct VertexData : IEquatable<VertexData>
    {
        public static readonly Type VertexDataType = typeof(VertexData);

        public Float3 Pos;
        public Float3[] Nml;
        public Float2[] Tex;
        public Float4[] Clrs;

        public VertexData(Float3 pos)
        {
            Pos = pos;
            Nml = null;
            Clrs = null;
            Tex = null;
        }

        public bool Equals(VertexData other)
        {
            return this == other;
        }
        public static bool operator ==(VertexData v1, VertexData v2)
        {
            if (v1.Pos == v2.Pos)
                if (ArrayEquals<Float3>(v1.Nml, v2.Nml))
                    if (ArrayEquals<Float2>(v1.Tex, v2.Tex))
                        if (ArrayEquals<Float4>(v1.Clrs, v2.Clrs))
                            return true;
            return false;
        }
        public static bool operator !=(VertexData v1, VertexData v2)
        {
            return !(v1 == v2);
        }
        public static bool ArrayEquals<T>(T[] a1, T[] a2)
        {
            if (a1 != null && a2 != null)
            {
                if (a1.Length == a2.Length)
                {
                    for (int i = 0; i < a1.Length; i++)
                        if (!a1[i].Equals(a2[i]))
                            return false;
                    return true;
                }
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType().Equals(VertexDataType))
                return ((VertexData)obj).Equals(this);
            return false;
        }
        public override int GetHashCode()
        {
            return Pos.GetHashCode() + Nml.GetHashCode() + Tex.GetHashCode() + Clrs.GetHashCode();
        }
        /*

        public static Complex operator +(Complex c1, Complex c2)
        {
            Return new Complex(c1.real + c2.real, c1.imaginary + c2.imaginary);
        }
        public static Complex operator +(Complex c1, Complex c2) =>
       new Complex(c1.real + c2.real, c1.imaginary + c2.imaginary);

        */
    }

}