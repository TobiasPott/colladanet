﻿using ColladaNET.Elements;
using System;
using System.Collections.Generic;

namespace ColladaNET
{
    /// <summary>
    /// class used to create collada instances
    /// </summary>
    public partial class ColladaFactory
    {

        #region Fields

        private float[] _positions = null;
        private float[] _normals = null;
        private float[] _texcoords = null;
        private float[] _vertexcolors = null;
        private int[] _triangles = null;

        private LibraryGeometries _libGeometries = new LibraryGeometries();
        private LibraryImages _libImages = new LibraryImages();
        private LibraryMaterials _libMaterials = new LibraryMaterials();
        private LibraryEffects _libEffects = new LibraryEffects();
        private LibraryControllers _libControllers = new LibraryControllers();
        private LibraryAnimations _libAnimations = new LibraryAnimations();
        private LibraryVisualScenes _libVisualScenes = new LibraryVisualScenes();
        #endregion

        #region Properties

        public float[] Positions
        {
            get { return _positions; }
            set { _positions = value; }
        }

        public float[] Normals
        {
            get { return _normals; }
            set { _normals = value; }
        }

        public float[] Texcoords
        {
            get { return _texcoords; }
            set { _texcoords = value; }
        }

        public float[] Vertexcolors
        {
            get { return _vertexcolors; }
            set { _vertexcolors = value; }
        }

        public int[] Triangles
        {
            get { return _triangles; }
            set { _triangles = value; }
        }

        #endregion


        /***
         * Create a storage variable to store unfinished COLLADA objects in
         * (this is necessary to pack multiple things into one one file without doing it at once)
         */

        private Collada _collada;
        public void Begin()
        {
            _collada = new Collada();
            _collada.asset = new Asset(new Asset.Contributor("Tobias Pott", "", "Tobias Pott 2012-" + DateTime.Now.Year, ""), DateTime.Now, DateTime.Now, new Asset.Unit(0.01f, "meter"), UpAxisType.Y_UP);
        }

        public Collada End()
        {
            _collada.libraries = new LibraryBase[] { _libImages, _libMaterials, _libEffects, _libGeometries, _libControllers, _libAnimations, _libVisualScenes };
            return _collada;
        }

        private void Clear()
        {
            _libGeometries = new LibraryGeometries();
            _libImages = new LibraryImages();
            _libMaterials = new LibraryMaterials();
            _libEffects = new LibraryEffects();
            _libVisualScenes = new LibraryVisualScenes();
            _collada.scene = null;
            _collada = null;
        }


        public LibraryGeometries LibraryGeometries
        { get { return _libGeometries; } }
        public void AddGeometry(Geometry geometry)
        {
            List<Geometry> geometries = _libGeometries.geometry;
            if (geometries == null)
                geometries = new List<Geometry>();

            geometries.Add(geometry); // COLLADAGeometry.GenerateGeoTriangles(name, matName, positions, normals, texcoords, vertexColors, triangles);
            _libGeometries.geometry = geometries;
        }

        public LibraryImages LibraryImages
        { get { return _libImages; } }
        public void AddImage(string id, string name, string imgSrc)
        {
            List<Image> images = _libImages.image;
            if (images == null)
                images = new List<Image>(); ;

            images.Add(new Image(id, name, imgSrc));
            _libImages.image = images;
        }

        public LibraryMaterials LibraryMaterials
        { get { return _libMaterials; } }
        public void AddMaterial(string id, string name, string fxInstUrl)
        {
            List<Material> materials = _libMaterials.material;
            if (materials == null)
                materials = new List<Material>();

            materials.Add(new Material(id, name, new InstanceEffect(fxInstUrl)));
            _libMaterials.material = materials;
        }

        public LibraryEffects LibraryEffects
        { get { return _libEffects; } }
        public void AddEffect(string id, string name, ColladaNET.FX.Profiles.Common shader)
        {
            List<Effect> effects = _libEffects.effect;
            if (effects == null)
                effects = new List<Effect>();

            effects.Add(new Effect(id, name, new ColladaNET.FX.Profiles.Common[] { shader }));
            _libEffects.effect = effects;
        }

        public LibraryControllers LibraryControllers
        { get { return _libControllers; } }
        public void AddController(Controller controller)
        {
            List<Controller> controllers = _libControllers.controller;
            if (controllers == null)
                controllers = new List<Controller>();

            controllers.Add(controller); // COLLADAGeometry.GenerateGeoTriangles(name, matName, positions, normals, texcoords, vertexColors, triangles);
            _libControllers.controller = controllers;
        }

        public LibraryAnimations LibraryAnimations
        { get { return _libAnimations; } }

        public void AddAnimation(Animation animation)
        {
            List<Animation> animations = _libAnimations.animation;
            if (animations == null)
                animations = new List<Animation>();

            animations.Add(animation);
            _libAnimations.animation = animations;
        }

        public LibraryVisualScenes LibraryVisualScenes
        { get { return _libVisualScenes; } }
        public VisualScene AddVisualScene(string sceneName)
        {
            List<VisualScene> visualScenes = _libVisualScenes.visual_scene;
            if (visualScenes == null)
                visualScenes = new List<VisualScene>();

            VisualScene scene = new VisualScene(sceneName, sceneName);
            visualScenes.Add(scene);
            _libVisualScenes.visual_scene = visualScenes;
            return scene;
        }

        public void SetSceneInstanceVisualScene(string name, string sid, string url)
        {
            if (_collada.scene == null)
                _collada.scene = new Scene();
            _collada.scene.instance_visual_scene = new InstanceVisualScene(name, sid, url);
        }
    }
}
